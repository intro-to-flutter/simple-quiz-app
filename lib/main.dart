import 'package:flutter/material.dart';
import 'package:simple_quiz_app/data.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple Quiz App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Simple Quiz App"),
        ),
        body: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Widget> moods = [];
  int questionListIndex = 0;
  int point = 0;

  void checkAnswerAndNextQuestion(bool answer) {
    setState(() {
      if (answer == questionList[questionListIndex].answer) {
        moods.add(correct);
        point = point + 10;
      } else {
        moods.add(unCorrect);
      }
      questionListIndex++;

      if (questionListIndex == 10) {
        questionListIndex = 0;
        moods = [];

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: new Text("Oyun Bitti"),
                content: new Text("Puanınız: $point/100"),
                actions: [
                  new FlatButton(
                      onPressed: () {
                        point = 0;
                        Navigator.of(context).pop();
                      },
                      child: new Text("Başa Dön"))
                ],
              );
            });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.lightBlue.shade100,
        child: Column(
          children: [
            Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Center(
                        child: Text(
                          questionList[questionListIndex].question,
                          style: TextStyle(fontSize: 20),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                )),
            Wrap(
              runSpacing: 3,
              spacing: 3,
              children: moods,
              alignment: WrapAlignment.start,
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  children: [
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          color: Colors.red,
                          child: Icon(
                            Icons.thumb_down,
                            size: 30,
                          ),
                          onPressed: () {
                            checkAnswerAndNextQuestion(false);
                          }),
                    )),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          color: Colors.green,
                          child: Icon(
                            Icons.thumb_up,
                            size: 30,
                          ),
                          onPressed: () {
                            checkAnswerAndNextQuestion(true);
                          }),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

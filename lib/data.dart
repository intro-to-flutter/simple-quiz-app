
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Widget correct = Icon( Icons.mood_outlined, color: Colors.green);
const Widget unCorrect = Icon( Icons.mood_bad_sharp, color: Colors.red);

class Question {
  String question;
  bool answer;

  Question(String question, bool answer ){
    this.question = question;
    this.answer = answer;
  }
}

var question1 = new Question("İstanbulu Fatih Sultan Mehmet fethetmiştir.", true);
var question2 = new Question("İnce Mehmed Orhan Kemal'in bir romanıdır.", false);
var question3 = new Question("Kerem Gibi şiiri Nazım Hikmet'e aittir.", true);
var question4 = new Question("Sivas yüz ölçümü bakımından en büyük ilimizdir.", false);
var question5 = new Question("1 asal sayıdır", false);
var question6 = new Question("Flutter ile Web uygulama geliştirilebilir.", true);
var question7 = new Question("Cemal Gürsel başbakanlık yapmıştır.", false);
var question8 = new Question("Türkiye'nin uçak gemisi yoktur.", true);
var question9 = new Question("Amerika'yı Macellan keşfetmiştir.", false);
var question10 = new Question("3 Idiot bir Hollywood filmidir.", false);

List<Question> questionList = [
  question1,
  question2,
  question3,
  question4,
  question5,
  question6,
  question7,
  question8,
  question9,
  question10
];
